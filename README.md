# soal-shift-sisop-modul-1-A05-2022

## Nomor 1

Source code : [disini](https://gitlab.com/hansenidden18/soal-shift-sisop-modul-1-a05-2022/-/tree/main/soal1)

### Deskripsi Soal

Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

#### 1.A

Membuat ```register.sh``` untuk membuat user baru dan ```main.sh``` untuk login dan melakukan command.

Untuk register menggunakan perintah 
```
sudo useradd -m -p "$password" "$username"
```
yang kan menerima password dan username yang diinginkan dan password harus sudah di enkripsi.

Lalu username dan password akan disimpan ke dalam file ```./users/user.txt```. dengan pertama akan dicek apabila file sudah ada, bila belum akan dibuat.
```
if [ ! -d "$filename" ]
then
 mkdir -p  "./users"
fi

if [ ! -f "$filename" ]
then
 touch "$filename"
fi
```

Lalu, untuk login user yang akan dipakai dan passwordnya. Karena hanya ingin mengecek maka menggunakan perintah
```
su -c true "$username"
```
yang akan mengeluarkan true bila password benar dan salah bila password salah.

#### 1.B

Mengecek password yang dibuat apakah sudah memenuhi kriteria yang ada, minimal 8 karakter dengan minimal 1 huruf besar, 1 huruf kecil, alphanumeric, dan password tidak sama dengan username.

Untuk ini saya menggunakan perintah checking
```
FAIL=no

[[ $(#password) -ge 8 ]] || FAIL=yes

echo $password | grep -q "[A-Z]" || FAIL=yes

echo $password | grep -q "[a-z]" || FAIL=yes

echo $password | grep -q "[0-9]" || FAIL=yes

FAIL=yes
echo $password | grep -q "[^a-zA-Z0-9]" || FAIL=no

[[ ${password} != ${username}]] || FAIL=yes

[[ ${FAIL} == "no" ]] && break

echo "Password do not match criteria"
```

#### 1.C

Membuat log dan menyimpan pada ```log.txt``` dengan format
```
YYYY/MM/DD hh:mm:ss MESSAGE
```
Pertama, akan dicek di register.sh saja karena user yang ingin memasukan pasti user baru.
```
if [ ! -f "$log" ]
then
 touch "$log"
fi
```

Pertama, untuk tanggal dan jamnya digunakan fungsi date dengan format
```
"$(date '+%Y/%m/%d %H:%M:%S') MESSAGE" >> $log
```
yang akan diubah ```MESSAGE``` sesuai soal yang ada.

#### 1.D

User dapat untuk mengetikkan 2 command yang dimana:
- ```dl N``` (N = jumlah gambar yang ingin diunduh)
 Gambar akan dimasukan ke dalam file yang bernama ```YYYY-MM-DD_USERNAME```. Dan format penamaan gambar ```PIC_XX```.
 Dan hasilnya akan di zip dan dipassword sesuai password user yang mengunduh.
<<<<<<< README.md

=======
 ```
 zipname=result$("${file}_${username}").zip
 
 dir="${file}_${username}/"
 pas=$(awk -v X="$username" '$1==X {print $2}' ./users/user.txt)
 
 sudo zip --password "$pas" -r "${file}_${username}.zip" $dir 
 ```
 Disini tidak mengunzip file yang sudah ada dan menambahkannya karena pasti akan hilang karena penamaan yang sama ```PIC_XX```.
- att
yang akan mengeluarkan berapa kali percobaan login yang dilakukan user gagal ataupun berhasil. Menggunakan bantuan awk dengan perintah
```
echo | awk -v X="$username" 'BEGIN{} {if($3=="LOGIN:" && ($6==X||$10==X)) ++n} END {print n}' log.txt
```

## Nomor 2

### Deskripsi Soal

Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. 
Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk
bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

#### 2.A

Membuat folder yang bernama forensic_log_website_daffainfo_log.
```
mkdir forensic_log_website_daffa_log
```

#### 2.B

Berapa rata-rata request per jam yang dikirimkan penyerang ke
website. 
Dengan menggunakan awk 
```
awk '
BEGIN{FS=":"}
END{
    print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "request per jam\n"
} ' /home/azhar/sisop/test/log_website_daffainfo.log
```
Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama
ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```
.. >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/ratarata.txt
```
#### 2.C

Menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut.
Pertama jadikan semua ip sebagai indeks array `{arr[$1]++}`, nantinya apabila ada ip yang sama maka data di array akan ditambah
```
awk '
BEGIN{FS=":"}
{arr[$1]++}
```
Setelah itu, cari ip dan jumlah request terbanyak menggunakan `for` lalu print hasilnya
```
END{
    ip = $1
    max = 0
    for (i in arr){
        if(max < arr[i]){
            ip = i
            max = arr[i]
        }
    }
    print "IP yang paling banyak mengakses server adalah:", ip, "sebanyak", max, "requests\n"
} 
' /home/azhar/sisop/test/log_website_daffainfo.log
```
Lalu hasil tersebut dimasukan kedalam file result.txt di folder yang sudah dibuat sebelumnya
```
.. >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt
```

#### 2.D

Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya
request, berapa banyak requests yang menggunakan user-agent curl?
Pertama menggunakan `/curl/` setelah itu `n` ditambahkan 
```
awk '
BEGIN{FS=":"}
/curl/ {n++}
```
Lalu print hasil `n` tersebut
```
END {
   print "Ada", n, "request yang menggunakan curl sebagai user-agent\n"
} ' /home/azhar/sisop/test/log_website_daffainfo.log
```
Setelah itu hasilnya dimasukkan kedalam folder forensic_log_website_daffainfo_log dan di dalam file result.txt
```
.. >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt
```
#### 2.E

Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin
mencari tahu daftar IP yang mengakses website pada jam tersebut. 
Dengan menggunakan pemisah `:` letak jam pada argumen ketiga `$3`
```
awk -F":" '$3 == "02" {print $1" Jam 2 pagi"}' /home/azhar/sisop/test/log_website_daffainfo.log
```
Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat
sebelumnya.
```
.. >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt
```

## Nomor 3

### Deskripsi Soal

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

#### 3.A

Soal : Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

Pertama, membuat directory log jika user belum memiliki directory log.
```
mkdir log
```
Untuk menyimpan file log ke dalam metrics_{YmdHms}.log, maka dibuat variabel untuk menyimpan nama file
```
filename="metrics_"$(date "+%Y%m%d%H%m%s")".log"
```
Hasil dari command `free -m` disimpan di sebuah array agar bisa diakses menggunakan looping.
```
my_array=($(free -m))
```
Isi di dalam array tersebut diakses menggunakan loop untuk menghasilkan log dengan contoh:
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```
for ((c=0;c<=5;c++))
do
  echo -n "mem_${my_array[$c]}," >> ./log/$filename
done

for ((c=0;c<=2;c++))
do
  echo -n "swap_${my_array[$c]}," >> ./log/$filename
done

echo -n "path,path_size" >> ./log/$filename

printf "\n" >> ./log/$filename

for ((c=7;c<=12;c++))
do
  echo -n "${my_array[$c]}," >> ./log/$filename
done

for ((c=14;c<=16;c++))
do
  echo -n "${my_array[$c]}," >> ./log/$filename
done
```
Hal sama dilakukan dengan command `du -sh <target_path>`. Target path yang dimasukkan adalah /home/{user}/.
```
disk=($(du -sh $HOME))

echo "${disk[1]},${disk[0]}" >> ./log/$filename
```

#### 3.B

Soal : Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

Untuk dapat menjalankan script pada waktu tertentu secara otomatis, maka digunakan cron. Untuk mengakses cron, ketik `crontab -e`. Lalu, karena script `minute_log.sh` ingin dijalankan pada setiap menit, maka command ini dimasukkan pada cron
```
* * * * * ./minute_log.sh > /dev/null 2>&1
``` 

#### 3.C

Soal : Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

Untuk menyimpan file log ke dalam metrics_agg_{YmdH}.log, maka dibuat variabel untuk menyimpan nama file
```
date1hourago=$(date -d '1 hour ago' "+%Y%m%d%H")

filename="metrics_agg_$date1hourago.log"
```
Dikarenakan yang ingin diproses adalah file log satu jam terakhir, maka kita perlu mencari file log yang dibuat selama 1 jam kebelakang dan menghapus bagian tulisan 'mem_total,mem_used' dan lain-lain. Sebelum dihapus, satu baris tulisan tersebut dipindahkan ke dalam file metrics_agg_{YmdH}.log
```
find /$HOME/log -name "metrics_20*" -mmin -60 -exec cat {} + > /$HOME/log/temp_hourly.log

sed -n '1p' /$HOME/log/temp_hourly.log > /$HOME/log/$filename

sed -i '/^mem/d' /$HOME/log/temp_hourly.log
```
Lalu, untuk mencari minimum dari suatu kolom, maka commandnya sebagai berikut
```
echo -n "Minimum," >> /$HOME/log/$filename 

awk -F "\"*,\"*" '{ORS=","}min=="" || $1 < min {min=$1} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $2 < min {min=$2} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $3 < min {min=$3} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $4 < min {min=$4} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $5 < min {min=$5} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $6 < min {min=$6} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $7 < min {min=$7} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $8 < min {min=$8} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}min=="" || $9 < min {min=$9} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
echo -n "$HOME," >> /$HOME/log/$filename 
awk -F "\"*,\"*" 'min=="" || $11 < min {min=$11} END {print min}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename
```
Lalu, untuk mencari maximum dari suatu kolom, maka commandnya sebagai berikut
```
echo -n "Maximum," >> /$HOME/log/$filename 

awk -F "\"*,\"*" '{ORS=","}max=="" || $1 > max {max=$1} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $2 > max {max=$2} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $3 > max {max=$3} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $4 > max {max=$4} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $5 > max {max=$5} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $6 > max {max=$6} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $7 > max {max=$7} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $8 > max {max=$8} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}max=="" || $9 > max {max=$9} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
echo -n "$HOME," >> /$HOME/log/$filename 
awk -F "\"*,\"*" 'max=="" || $11 > max {max=$11} END {print max}' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
```
Terakhir, untuk mencari average dari suatu kolom, maka commandnya sebagai berikut
```
echo -n "Average," >> /$HOME/log/$filename 

awk -F "\"*,\"*" '{ORS=","}{ total+=$1 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$2 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$3 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$4 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$5 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$6 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$7 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$8 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
awk -F "\"*,\"*" '{ORS=","}{ total+=$9 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename
echo -n "$HOME," >> /$HOME/log/$filename  
awk -F "\"*,\"*" '{ total+=$11 } END { print total/NR }' /$HOME/log/temp_hourly.log >> /$HOME/log/$filename 
```
Untuk dapat menjalankan script pada waktu tertentu secara otomatis, maka digunakan cron. Untuk mengakses cron, ketik `crontab -e`. Lalu, karena script `aggregate_minutes_to_hourly_log.sh` ingin dijalankan pada setiap jam, maka command ini dimasukkan pada cron
```
0 * * * * ./aggregate_minutes_to_hourly_log.sh > /dev/null 2>&1
``` 
Command tersebut menunjukkan bahwa script akan dijalankan setiap menit ke 0.

#### 3.D

Soal : Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Ditambahkan pada akhir bash script minute_log.sh dan aggregate_minutes_to_hourly_log.sh, agar file log hanya dapat diakses oleh user
```
chmod 700 ./log/$filename
```
