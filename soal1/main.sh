#!/bin/bash

log="log.txt"
file=$(date '+%Y-%m-%d')

echo "P Login"
read -p "Input username: " username

if su -c true "$username";then
 echo "Correct password"
 echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> $log
else
 echo "Wrong password"
 echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user  $username" >> $log
 exit 1
fi


if [ ! -d $file ];then
 sudo mkdir -p "./${file}_${username}"
fi

read -p "command: " com

if grep -q "dl" <<< "$com";then
 number=$(echo $com | grep -o -E '[0-9]+')

 for i in $(seq -f "%02g" 01 $number)
 do
  sudo wget -O "./${file}_${username}/PIC_${i}" "https://loremflickr.com/320/240"
 done
 zipname=result$("${file}_${username}").zip
 dir="${file}_${username}/"
 pas=$(awk -v X="$username" '$1==X {print $2}' ./users/user.txt)
 sudo zip --password "$pas" -r "${file}_${username}.zip" $dir 

elif grep -q "$com" <<< "att";then

 echo | awk -v X="$username" 'BEGIN{} {if($3=="LOGIN:" && ($6==X||$10==X)) ++n} END {print n}' log.txt
fi

echo "Done"
