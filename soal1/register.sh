#!/bin/bash

filename="./users/user.txt"
log="log.txt"

if [ ! -d "$filename" ]
then
 mkdir -p  "./users"
fi

if [ ! -f "$filename" ]
then
 touch "$filename"
fi

if [ ! -f "$log" ]
then
 touch "$log"
fi

echo -e "Please don't register existed username"
read -p "Input username: " username

while true; do
	echo -e "\nPlease input  your password"
	read -s -p "Input password: " password
	echo

	FAIL=no

	[[ ${#password} -ge 8 ]] || FAIL=yes

	echo $password | grep -q "[A-Z]" || FAIL=yes

	echo $password | grep -q "[0-9]" || FAIL=yes

	echo $password | grep -q "[a-z]" || FAIL=yes
	
	FAIL=yes
	echo $password | grep -q "[^a-zA-Z0-9]" || FAIL=no

	[[ ${password} != ${username} ]] || FAIL=yes

	[[ ${FAIL} == "no" ]] && break

	echo "Password do not match criteria"
done

pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
sudo useradd -m -p "$pass" "$username"
if [ $? -eq 0 ]; then
  echo "User has been added to system!"
  echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> $log 
  echo $username $password >> $filename 
else
 echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> $log  
 echo "Failed to add user!"
fi

