#!/bin/bash

mkdir forensic_log_website_daffainfo_log

awk '
BEGIN{FS=":"}
END{
    print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "request per jam\n"
}
' /home/azhar/sisop/test/log_website_daffainfo.log >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/ratarata.txt

awk '
BEGIN{FS=":"}
{arr[$1]++}
END{
    ip = $1
    max = 0
    for (i in arr){
        if(max < arr[i]){
            ip = i
            max = arr[i]
        }
    }
    print "IP yang paling banyak mengakses server adalah:", ip, "sebanyak", max, "requests\n"
} 
' /home/azhar/sisop/test/log_website_daffainfo.log >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt

awk '
BEGIN{FS=":"}
/curl/ {n++}
END {
   print "Ada", n, "request yang menggunakan curl sebagai user-agent\n"
} 
' /home/azhar/sisop/test/log_website_daffainfo.log >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt

awk -F":" '$3 == "02" {print $1" Jam 2 pagi"}' /home/azhar/sisop/test/log_website_daffainfo.log >> /home/azhar/sisop/test/forensic_log_website_daffainfo_log/result.txt
