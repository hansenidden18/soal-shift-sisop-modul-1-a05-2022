#!/bin/bash

mkdir log

filename="metrics_"$(date "+%Y%m%d%H%m%s")".log"

my_array=($(free -m))

for ((c=0;c<=5;c++))
do
  echo -n "mem_${my_array[$c]}," >> ./log/$filename
done

for ((c=0;c<=2;c++))
do
  echo -n "swap_${my_array[$c]}," >> ./log/$filename
done

echo -n "path,path_size" >> ./log/$filename

printf "\n" >> ./log/$filename

for ((c=7;c<=12;c++))
do
  echo -n "${my_array[$c]}," >> ./log/$filename
done

for ((c=14;c<=16;c++))
do
  echo -n "${my_array[$c]}," >> ./log/$filename
done

disk=($(du -sh $HOME))

echo "${disk[1]},${disk[0]}" >> ./log/$filename

chmod 700 ./log/$filename
